#!/bin/bash


RAMSES_HDF5=$HOME/soft/ramses_hdf5/ramses_hdf5
#RAMSES_HDF5=ramses_hdf5/ramses_hdf5


MP0=0  # initial mass field exists??
MET=1  # metallicity field exists?
PREC=4 # want to save the data space? if not, set it to 8
NEW_RAMSES=.true.


OPTION="-mp0 ${MP0} -met ${MET} -prec ${PREC} -new ${NEW_RAMSES}"


list=`ls -d output_?????`

if [ ! -e 'HDF5' ];then
	mkdir HDF5
fi

for i in $list
do
	SNOUT=${i//[^0-9]}
	OUTPUT=HDF5/ramses_${SNOUT}.h5

	if [ ! -e ${OUTPUT} ]; then
		COMM="${RAMSES_HDF5} -nout ${SNOUT} -out ${OUTPUT} ${OPTION}"
		echo $COMM
		$COMM
	fi
done
#ramses_hdf5 -nout 5 
