import numpy as np
import h5py
import os
import sys
import subprocess
import platform





######################################################################################################################
class rd_ram(object):
    """
;+
; NAME:
;    RD_RAM
;
; PURPOSE:
;    This procedure reads HDF5 generated with RAMSES_HDF5.F90
;       and convert into something easy to handle
;
; CATEGORY:
;    Scientific data 
;
; CALLING SEQUENCE:
;   cls_data = rd_ram()
;   cls_data.readAll(file=PATH2FILE)
;
;   cls_data = rd_ram(path=PATH2DIRECTORY)
;   cls_data.readAll(nout=NOUT)
; 
; DATA ACCESS :
;   cls_data.info[KEY]
;   cls_data.cell[KEY]
;   cls_data.star[KEY]
;   cls_data.part[KEY]
;
; COMMON BLOCKS:
;    None.
;
; REQUIRED INPUTS:
;    NOUT: snapshot number
;
; OPTIONAL INPUTS:
;    PATH: directory of HDF5
;    FILE: name of HDF5
;    VERBOSE: print messages
;
; MODIFICAITON HISTORY:
;    Written by: Chongsam Na, Oct/18/2019
;-
"""


    #**************************************************************************************************************
    def __init__(self, *args, **kwargs):
        ### Class initialization

        #-----------------------------------------
        # Parameter check
        #-----------------------------------------
        self.verbose = True
        if "verbose" in kwargs:
            self.verbose = kwargs["verbose"]

        self.path = None
        if "path" in kwargs:
            self.path = kwargs["path"]
        else:
            self.path = 'HDF5'

        self.nout = -1
        self.file = None
        self.info = None
        self.cell = None
        self.star = None
        self.part = None

    #**************************************************************************************************************


    #**************************************************************************************************************
    def readAll(self, nout=None, file=None, silent=None):
        if silent is not None:
            self.verbose = not silent

        #-----------------------------------------
        # input check
        #-----------------------------------------
        if nout is not None:
            self.nout = nout
        else:
            if file is None:
                err_message='>>> ERROR: NOUT is missing'
                self.sysErrMessage(err_message)
        
        #-----------------------------------------
        # guess file name
        #-----------------------------------------
        if file is not None:
            self.file = file
        else:
            snout = "%05d"%self.nout
            HDF5_file = self.path+'/ramses_'+snout+'.h5'
            self.file = HDF5_file


        #-----------------------------------------
        # check if file exists
        #-----------------------------------------
        if not os.path.exists(self.file):
            err_message='>>> ERROR: not found '+self.file
            self.sysErrMessage(err_message)
        
        #-----------------------------------------
        # open HDF5 file
        #-----------------------------------------
        if self.verbose:
            print(">>> reading..."+self.file)
        h5 = h5py.File(self.file, 'r')

        
        #-----------------------------------------
        # retrieve info and re-structure 
        #-----------------------------------------
        info_old = h5["info"][...]       #Structure
        header   = h5["cell"].attrs.get('header')

        self.info = {}
        for key in info_old.dtype.names:
            self.info[key] = info_old[key][0]       #info_old[key] is an array with 1 element
                                                    #data type is sucessfully restored?

        self.info['boxkpc'] = self.info["boxpc"]/1.e3
        self.info['pc']     = self.info["boxpc"]
        self.info['msun']   = self.info["unit_msun"]

        self.info['itemp']  = np.int64(4)       #Correspond to IDL Long type
        self.info["header"] = header
        
        
        #-----------------------------------------
        # determine itemp
        #-----------------------------------------
        for i in range(len(header)):
            text = header[i]
            if text.find(": thermal") > 0:
                self.info["itemp"] = i
            if text.find(" pressure") > 0:
                self.info["itemp"] = i

        #-----------------------------------------
        # retrieve basic info for cell
        #-----------------------------------------
        nvar   = h5["/cell/nvar"].value[0]
        ncell  = len(h5["/cell/x"].value)
        dtype  = h5["/cell/x"].value[0].dtype
        self.cell = {}
        self.cell["ncell"] = len(h5["/cell/x"].value)
        self.cell["var"  ] = np.zeros((ncell, nvar), dtype=dtype)     #Cuation : [index of cell, index of var]
        
        #-----------------------------------------
        # fetch hydro data
        #-----------------------------------------
        list_keys = ["x", "y", "z", "dx"]
        for key in list_keys:
            self.cell[key] = h5["/cell/%s"%key][...]
        
        for ivar in range(0, nvar):
            self.cell["var"][:,ivar] = h5["/cell/var%05d"%(ivar+1)][...]

        #-----------------------------------------
        # sanity check for hydro data
        #-----------------------------------------
        for ivar in range(nvar):
            ok = np.where(np.isfinite(self.cell["var"][:,ivar]) == False)[0]
            if len(ok) > 0:
                err_message = '>>> ERROR: NaN found in ivar=%d  ncell=%d'%(ivar, ok[0])
                if len(ok) > 1:
                    print(">>> Indices of cells with NaN value are")
                    print(ok)
                self.sysErrMessage(err_message)


        #-----------------------------------------
        # retrieve basic info for part
        #-----------------------------------------
        ntot = len(h5["/part/id"].value)
        list_keys = h5["part"].keys()
        met_exist = ("zp"  in list_keys)
        mp0_exist = ("mp0" in list_keys)

        #-----------------------------------------
        # fetch data space for star and DM
        #-----------------------------------------


        #-----------------------------------------
        # prepare data space for star and DM
        #-----------------------------------------
        tp = h5["/part/tp"][...]
        idx_star = np.where( np.abs(tp) > 1e-20 )[0]
        idx_part = np.where( np.abs(tp) < 1e-20 )[0]
        nstar = len(idx_star)
        npart = len(idx_part)
        do_star = (nstar > 0)
        do_part = (npart > 0)
        dtype_pos = h5["/part/xpart"].value[0].dtype
        dtype_vel = h5["/part/vxp"  ].value[0].dtype

        #-----------------------------------------
        # take care of stars first
        #-----------------------------------------
        self.star = {}
        self.star["npart"] = nstar
        if do_star:
            self.star["xp"] = np.zeros((nstar, 3), dtype=dtype_pos)   #np.double
            self.star["vp"] = np.zeros((nstar, 3), dtype=dtype_vel)   #np.float

            self.star["xp"][:,0] = h5["/part/xpart"].value[idx_star]
            self.star["xp"][:,1] = h5["/part/ypart"].value[idx_star]
            self.star["xp"][:,2] = h5["/part/zpart"].value[idx_star]
            self.star["vp"][:,0] = h5["/part/vxp"  ].value[idx_star]
            self.star["vp"][:,1] = h5["/part/vyp"  ].value[idx_star]
            self.star["vp"][:,2] = h5["/part/vzp"  ].value[idx_star]
            self.star["mp"]      = h5["/part/mp"   ].value[idx_star]
            self.star["tp"]      = h5["/part/tp"   ].value[idx_star]
            self.star["id"]      = h5["/part/id"   ].value[idx_star]
            if met_exist: self.star["zp" ] = h5["/part/zp" ].value[idx_star]
            if mp0_exist: self.star["mp0"] = h5["/part/mp0"].value[idx_star]


        #-----------------------------------------
        # take care of other particles
        #-----------------------------------------
        self.part = {}
        self.part["npart"] = npart
        if do_part:
            self.part["xp"] = np.zeros((npart, 3), dtype=dtype_pos)   #np.double
            self.part["vp"] = np.zeros((npart, 3), dtype=dtype_vel)    #np.float

            self.part["xp"][:,0] = h5["/part/xpart"].value[idx_part]
            self.part["xp"][:,1] = h5["/part/ypart"].value[idx_part]
            self.part["xp"][:,2] = h5["/part/zpart"].value[idx_part]
            self.part["vp"][:,0] = h5["/part/vxp"  ].value[idx_part]
            self.part["vp"][:,1] = h5["/part/vyp"  ].value[idx_part]
            self.part["vp"][:,2] = h5["/part/vzp"  ].value[idx_part]
            self.part["mp"]      = h5["/part/mp"   ].value[idx_part]
            self.part["tp"]      = h5["/part/tp"   ].value[idx_part]
            self.part["id"]      = h5["/part/id"   ].value[idx_part]



        h5.close()
    #**************************************************************************************************************


    #**************************************************************************************************************
    def sysErrMessage(self, message):
        self.col='\033[1;31m' #red
        self.NC='\033[0m'
        #print(message)
        comm = 'echo -e "'+self.col+message+self.NC+'"'

        if platform.system() == "Windows":  
            os.system(comm)
        else:
            os.popen(comm)
        exit()
    #**************************************************************************************************************
        


######################################################################################################################




######################################################################################################################
### Test script
if __name__ == "__main__":
    # Example 1
    testFile = "E:/0_PracticeCode/RamsesHDF5/HDF5/ramses_0001.h5"
    cls_data = rd_ram()
    cls_data.readAll(file=testFile)
    print("The total number of cells is %d"%cls_data.cell["ncell"])

    # Example 2
    testPath = "E:/0_PracticeCode/RamsesHDF5/HDF5/"
    cls_data = rd_ram(path=testPath)
    cls_data.readAll(nout=1)
    print("The total number of cells is %d"%cls_data.cell["ncell"])
######################################################################################################################