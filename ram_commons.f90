MODULE ram_commons
  implicit none

! KIND parameters
  INTEGER, PARAMETER :: i2b = SELECTED_INT_KIND(4)     ! mapping to INTEGER*2
  INTEGER, PARAMETER :: i4b= SELECTED_INT_KIND(9)     ! mapping to INTEGER*4
  INTEGER, PARAMETER :: i8b = SELECTED_INT_KIND(18)   ! mapping to INTEGER*8
  INTEGER, PARAMETER :: sp = SELECTED_REAL_KIND(6,37)   ! mapping to REAL*4
  INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(15,307) ! mapping to REAL*8

  real(KIND=dp)::xmin=0,xmax=1,ymin=0,ymax=1,zmin=0,zmax=1
  integer(KIND=i4b)::silent=0,nout,lmax=0
  integer(KIND=i4b)::nvarh,ndim_part,nvar_rt
  logical::verbose=.true. 
  logical::megaverbose=.false. 
  CHARACTER(LEN=256) :: outfich = "ramses.h5" ! File name
  CHARACTER(LEN=256) :: nomfich,repository=''

  ! particle variable
  INTEGER(KIND=i4b)::npart
  logical::do_metal=.true.
  logical::do_initm=.false.
  logical::do_sngl=.true.
  logical::new_ramses=.false.
  integer::meta=1,initm=0,prec=1
!  real(KIND=dp),dimension(:,:),allocatable :: xp,vp
  real(KIND=dp),dimension(:),allocatable :: xp,yp,zp
  real(KIND=dp),dimension(:),allocatable :: vxp,vyp,vzp
  real(KIND=dp),dimension(:),allocatable :: mp,tp,metp
  integer(KIND=i4b),dimension(:),allocatable :: id, ll
  real(KIND=dp),dimension(:),allocatable :: mp0
  real(KIND=sp),allocatable :: ss(:)
  real(KIND=dp),allocatable :: xx(:)

  ! cell variables
  integer(KIND=i4b)::nleaf=0,nleaf_rt=0,ncpu
  real(kind=dp),allocatable,dimension(:)::xcell,ycell,zcell,dxcell 
  real(kind=dp),allocatable,dimension(:,:)::varcell 
  real(kind=dp),allocatable,dimension(:)::x_rt,y_rt,z_rt,dx_rt 
  real(kind=dp),allocatable,dimension(:,:)::var_rt 

  ! header info
  INTEGER, PARAMETER :: header_len = 80
  CHARACTER(LEN=header_len),allocatable :: varnames(:)

  ! info variable
  type info_type
    real(kind=dp)::unit_d
    real(kind=dp)::unit_l
    real(kind=dp)::unit_t
    real(kind=dp)::unit_v
    real(kind=dp)::unit_nH
    real(kind=dp)::unit_T2
    real(kind=dp)::boxlen
    real(kind=dp)::aexp
    real(kind=dp)::time
    real(kind=dp)::unit_msun
    real(kind=dp)::boxpc
    real(kind=dp)::H0
    real(kind=dp)::omega_m
    real(kind=dp)::omega_l
    real(kind=dp)::omega_k
    real(kind=dp)::omega_b
    integer(kind=i4b)::lmax
    integer(kind=i4b)::lmin
    integer(kind=i4b)::ndim
    integer(kind=i4b)::ncpu
  end type info_type

  type(info_type),TARGET :: info(1)

  ! rtinfo variable
  type rtinfo_type
    integer(kind=i4b)::nRTvar
    integer(kind=i4b)::nIons
    integer(kind=i4b)::nGroups
    integer(kind=i4b)::iIons
    real(kind=dp)::X_fraction
    real(kind=dp)::Y_fraction
    real(kind=dp)::unit_np
    real(kind=dp)::unit_pf
    real(kind=dp)::rt_c_frac
    real(kind=dp)::n_star
    real(kind=dp)::T2_star
    real(kind=dp)::g_star
  end type rtinfo_type

  type(rtinfo_type),TARGET :: rtinfo(1)



  ! ramses units
  real(kind=dp)::pc2cm=3.08d18
  real(kind=dp)::mproton=1.66d-24
  real(kind=dp)::msun2g=1.989d33
  real(kind=dp)::mH=1.6600000d-24
  real(kind=dp)::kB=1.38D-16

  integer(KIND=i4b)::ncpu_read
  real(KIND=8),dimension(:),allocatable::bound_key
  logical,dimension(:),allocatable::cpu_read
  integer,dimension(:),allocatable::cpu_list


  contains
!------------------------------------------
  subroutine create_cell_variables
!------------------------------------------
      if (allocated(xcell)) then
         write(*,*)'Fatal error: cell variables are already allocated'
         stop
      endif

      allocate(xcell(1:nleaf))
      allocate(ycell(1:nleaf))
      allocate(zcell(1:nleaf))
      allocate(dxcell(1:nleaf))
      allocate(varcell(1:nleaf,1:nvarh))

      xcell(:)=0.0
      ycell(:)=0.0
      zcell(:)=0.0
      dxcell(:)=0.0
      varcell(:,:)=0.0

  end subroutine create_cell_variables

!------------------------------------------
  subroutine kill_cell_variables
!------------------------------------------
      if (.not.allocated(xcell)) then
         write(*,*)'Fatal error: cell variables are not allocated'
         stop
      endif

      if(allocated(xcell)) deallocate(xcell)
      if(allocated(ycell)) deallocate(ycell)
      if(allocated(zcell)) deallocate(zcell)
      if(allocated(dxcell)) deallocate(dxcell)
      if(allocated(varcell)) deallocate(varcell)

  end subroutine kill_cell_variables

!------------------------------------------
  subroutine create_rt_variables
!------------------------------------------
      if (allocated(x_rt)) then
         write(*,*)'Fatal error: rt variables are already allocated'
         stop
      endif

      allocate(x_rt(1:nleaf_rt))
      allocate(y_rt(1:nleaf_rt))
      allocate(z_rt(1:nleaf_rt))
      allocate(dx_rt(1:nleaf_rt))
      allocate(var_rt(1:nleaf_rt,1:nvar_rt))
  end subroutine create_rt_variables

!------------------------------------------
  subroutine kill_rt_variables
!------------------------------------------
      if (.not.allocated(x_rt)) then
         write(*,*)'Fatal error: cell variables are not allocated'
         stop
      endif

      if(allocated(x_rt)) deallocate(x_rt)
      if(allocated(y_rt)) deallocate(y_rt)
      if(allocated(z_rt)) deallocate(z_rt)
      if(allocated(dx_rt)) deallocate(dx_rt)
      if(allocated(varcell)) deallocate(var_rt)

  end subroutine kill_rt_variables


!------------------------------------------
  subroutine read_params
!------------------------------------------
        
    implicit none
        
    integer       :: i,n 
    integer       :: iargc
    character(len=5)   :: opt 
    character(len=128) :: arg 
    LOGICAL       :: bad, ok
    character(len=5) :: snout
        
    n = iargc()
    if (n < 1) then
       print *, 'usage: ramses_hdf5  -nout  [nout]'
       print *
       print *, 'ex: ramses_hdf5 -nout 35'
       print *, ' ' 
       stop
    end if
        
    do i = 1,n,2
       call getarg(i,opt)
       if (i == n) then
          print '("option ",a2," has no argument")', opt 
          stop 2
       end if
       call getarg(i+1,arg)
       select case (trim(opt))
       case ('-inp')
          repository = trim(arg)
       case ('-out')
          outfich = trim(arg)
       case ('-nout')
          read (arg,*) nout
          call title(nout,snout)
          outfich = 'ramses_'//TRIM(snout)//'.h5'
          if(TRIM(repository).eq.'')then
             repository='output_'//TRIM(snout)//'/'
          endif
       case ('-xmi')
          read (arg,*) xmin
       case ('-xma')
          read (arg,*) xmax
       case ('-ymi')
          read (arg,*) ymin
       case ('-yma')
          read (arg,*) ymax
       case ('-zmi')
          read (arg,*) zmin
       case ('-zma')
          read (arg,*) zmax
       case ('-lma')
          read (arg,*) lmax
       case ('-sil')
          read (arg,*) silent
       case ('-met')
          read (arg,*) meta
       case ('-mp0')
          read (arg,*) initm
       case ('-prec')
          read (arg,*) prec
       case ('-new')
          read (arg,*) new_ramses
       case default
          print '("unknown option ",a2," ignored")', opt
       end select
    end do
  
    if(meta.eq.0) do_metal=.false.
    if(initm.eq.0)do_initm=.false.
    if(prec.ne.4) do_sngl =.false.

    write(*,*)
    write(*,*)'>>> RAMSES_HDF5:  output = '//TRIM(outfich)
 
    return
    
  end subroutine read_params

END MODULE ram_commons
!#####################################################
!#####################################################
!#####################################################
