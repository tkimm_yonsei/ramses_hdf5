

#===================================
# HDF5 compile guideline
#===================================
# cd ~/soft/hdf5-1.10.5/
# ./configure --prefix=/home/kimm/soft/hdf5-1.10.5/ --enable-fortran
# make
# make install



#===================================
#          USER SETTING         
#===================================

HDF5    = ${HOME}/soft/hdf5-1.10.5

FFLAGS  = -O0 -fbacktrace -g
FFLAGS  = -O2








#===================================
# DO NOT CHANGE AFTER THIS POINT
# UNLESS YOU ARE CONFIDENT
#===================================
FC      = ${HDF5}/bin/h5fc
LIBS    = -L${HDF5}/lib -lhdf5


OBJS = ram_commons.o  ram_utils.o


all: ramses_hdf5

ramses_hdf5: $(OBJS)
	$(FC) -o ramses_hdf5 ramses_hdf5.f90 $(OBJS)
	chmod +x ramses_hdf5

clean:
	rm -rf *.o *.mod

.SUFFIXES: .f90

%.o:  %.f90 
	$(FC) $(FFLAGS) $(OPTION) -c $< -o $@

