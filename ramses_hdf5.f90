!#####################################################
! Simple program that converts 
! RAMSES outputs in Fortran binary -> HDF5 
!
! Initial version: Taysun Kimm (tkimm@yonsei.ac.kr)
!                              13/Oct/2019
!#####################################################
PROGRAM RAMSES_HDF5

  USE HDF5
  USE ram_commons

  IMPLICIT NONE


! HDF5 parameters
  CHARACTER(LEN=8) :: dsetname = "var"     ! Dataset name
  CHARACTER(LEN=9) :: aname = "header"     ! Attribute name
  CHARACTER(LEN=4) :: groupname

  INTEGER(HID_T) :: file_id       ! File identifier
  INTEGER(HID_T) :: group_id      ! Group identifier
  INTEGER(HID_T) :: dset_id       ! Dataset identifier
  INTEGER(HID_T) :: dspace_id     ! Dataspace identifier
  INTEGER(HID_T) :: attr_id       ! Attribute identifier
  INTEGER(HID_T) :: aspace_id     ! Attribute identifier
  INTEGER(HID_T) :: atype_id      ! Attribute Dataspace identifier
  INTEGER(HID_T) :: derived_id    ! Derived Type identifier

  
  INTEGER(HSIZE_T), DIMENSION(1) :: data_dims=(/1/) ! Dataset dimensions
  INTEGER(HSIZE_T), DIMENSION(1) :: attr_dims=(/2/) ! Attribute dimensions
  INTEGER(HSIZE_T), DIMENSION(1) :: info_dims=(/1/) ! Attribute dimensions
  INTEGER(HSIZE_T), DIMENSION(2) :: data_dims_2d=(/1,1/) ! Dataset dimensions
  INTEGER     ::   rank = 1                        ! Dataset rank
  INTEGER     ::   arank = 1                       ! Attribute rank
  INTEGER(SIZE_T) :: attr_len      ! Length of the attribute string

  INTEGER     ::   error ! Error flag
  INTEGER     ::   ivar,i,idim
  INTEGER(SIZE_T) :: real_size, int_size, compound_size
  INTEGER(SIZE_T) :: offset
  CHARACTER(LEN=5) :: cvar
  LOGICAL     :: file_ok, okay
  TYPE(C_PTR) :: f_ptr

  INTEGER, PARAMETER::nname_part=4
  CHARACTER(LEN=3), DIMENSION(nname_part) :: dsetname_part=(/'mp ','tp ','zp ','mp0'/)



  ! Read parameters
  CALL read_params

  ! Initialize FORTRAN interface.
  CALL H5OPEN_F(error)

  ! Create a new file using default properties.
  CALL H5FCREATE_F(outfich, H5F_ACC_TRUNC_F, file_id, error)

  ! Read the hydro_file_descriptor
  call read_file_descriptor(file_ok)


  !------------------------------------------------
  ! Create a group named "/info" in the file.
  !------------------------------------------------
   
  ! Read info from ramses info text
  CALL read_info(file_ok)


  
  CALL H5TGET_SIZE_F(H5T_NATIVE_DOUBLE, real_size, error)
  CALL H5TGET_SIZE_F(H5T_NATIVE_INTEGER, int_size, error)



  !-------------------------------------------------------
  compound_size = real_size*16 + int_size*4  ! BE CAREFUL
  !-------------------------------------------------------

  ! Create a new daterrore
  CALL H5SCREATE_SIMPLE_F(RANK, info_dims, dspace_id, error)

  ! Create the memory data type
  CALL H5TCREATE_F(H5T_COMPOUND_F, compound_size, derived_id, error) 

  offset=0
  CALL H5TINSERT_F(derived_id, "unit_d", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_l", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_t", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_v", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_nH", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_T2", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "boxlen", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "aexp", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "time", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "unit_msun", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "boxpc", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "H0", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "omega_m", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "omega_l", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "omega_k", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "omega_b", offset, H5T_NATIVE_DOUBLE, error)
  offset=offset+real_size
  CALL H5TINSERT_F(derived_id, "lmin", offset, H5T_NATIVE_INTEGER, error)
  offset=offset+int_size
  CALL H5TINSERT_F(derived_id, "lmax", offset, H5T_NATIVE_INTEGER, error)
  offset=offset+int_size
  CALL H5TINSERT_F(derived_id, "ndim", offset, H5T_NATIVE_INTEGER, error)
  offset=offset+int_size
  CALL H5TINSERT_F(derived_id, "ncpu", offset, H5T_NATIVE_INTEGER, error)
  offset=offset+int_size


  ! Create the dataset.
  CALL H5DCREATE_F(file_id, "info", derived_id, dspace_id, dset_id, error)

  ! Write data to the dataset
  f_ptr = C_LOC(info(1))
  CALL H5DWRITE_F(dset_id, derived_id, f_ptr, error)

  ! Close dataspace
  CALL H5TCLOSE_F(derived_id,error)
  CALL H5DCLOSE_F(dset_id, error)
  CALL H5SCLOSE_F(dspace_id, error)






  !------------------------------------------------
  ! Create a group named "/cell" in the file.
  !------------------------------------------------

  ! Read leaf cells from ramses snapshot
  CALL read_leaf_cell

  groupname='cell'
  CALL H5GCREATE_F(file_id, groupname, group_id, error)

  data_dims(1)=1
  CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
  ! Create the dataset with default properties
  dsetname = 'nvar'
  CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_INTEGER, dspace_id, &
       dset_id, error)
  ! Write the dataset
  CALL H5DWRITE_F(dset_id, H5T_NATIVE_INTEGER, nvarh, data_dims, error)
  ! End access to the dataset and release resources used by it.
  CALL H5DCLOSE_F(dset_id, error)
  ! Terminate access to the data space.
  CALL H5SCLOSE_F(dspace_id, error)



  data_dims(1)=nleaf

  do i=1,4
     ! Create field for nvar
     CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)

     ! Create the dataspace
     if(i==1)dsetname='x'
     if(i==2)dsetname='y'
     if(i==3)dsetname='z'
     if(i==4)dsetname='dx'
     CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
          dset_id, error)

     ! Write the dataset
     select case (i)
        case (1) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, xcell, data_dims, error)
        case (2) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, ycell, data_dims, error)
        case (3) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, zcell, data_dims, error)
        case (4) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, dxcell, data_dims, error)
     end select

     CALL H5DCLOSE_F(dset_id, error)
     CALL H5SCLOSE_F(dspace_id, error)
  end do


 
  ! repeat for 'var'
  if(do_sngl) then
     allocate(ss(1:nleaf))
  else
     allocate(xx(1:nleaf))
  endif

  do ivar=1,nvarh

     call title(ivar,cvar)
     CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
     dsetname = 'var'//cvar

     if(do_sngl)then
        CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_REAL, dspace_id, &
             dset_id, error)
        ss(:) = sngl(varcell(:,ivar))  ! OK to overwrite
        CALL H5DWRITE_F(dset_id, H5T_NATIVE_REAL, ss, data_dims, error)
     else
        CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
             dset_id, error)
        xx(:) = varcell(:,ivar)  ! OK to overwrite 
        CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, xx, data_dims, error)
     endif
     CALL H5DCLOSE_F(dset_id, error)
     CALL H5SCLOSE_F(dspace_id, error)

     ! Sanity check
     if (ivar==1)then
        write(*,*) '>>> RAMSES_HDF5:  nH =', sngl(minval(varcell(:,1))*info%unit_nH) &
                                  , sngl(maxval(varcell(:,1))*info%unit_nH)
     endif
     if (ivar==5)then
        write(*,*) '>>> RAMSES_HDF5:  Tk =', sngl(minval(varcell(:,5)/varcell(:,1))*info%unit_T2) &
                                  , sngl(maxval(varcell(:,5)/varcell(:,1))*info%unit_T2) 
     endif

  end do



  ! Flush memory for cell variables
  call kill_cell_variables


  ! if file exists, record the information as attribute
  if(file_ok) then 
      attr_len = header_len
      attr_dims(1) = nvarh
      ! Create scalar data space for the attribute.
      CALL H5SCREATE_SIMPLE_F(arank, attr_dims, aspace_id, error)
    
      ! Create datatype for the attribute.
      CALL H5TCOPY_F(H5T_NATIVE_CHARACTER, atype_id, error)
      CALL H5TSET_SIZE_F(atype_id, attr_len, error)
    
      ! Create dataset attribute.
      CALL H5ACREATE_F(group_id, aname, atype_id, aspace_id, attr_id, error)
    
      ! Write the attribute data.
      CALL H5AWRITE_F(attr_id, atype_id, varnames, attr_dims, error)
  endif




  !------------------------------------------------
  ! Create a group named "/part" in the file.
  !------------------------------------------------

  ! Read particle info (DM+stars) from ramses snapshot
  CALL read_part

  groupname='part'
  CALL H5GCREATE_F(file_id, groupname, group_id, error)

  data_dims(1)=1
  CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
  ! Create the dataset with default properties
  dsetname = 'npart'
  CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_INTEGER, dspace_id, &
       dset_id, error)
  ! Write the dataset
  CALL H5DWRITE_F(dset_id, H5T_NATIVE_INTEGER, npart, data_dims, error)
  ! End access to the dataset and release resources used by it.
  CALL H5DCLOSE_F(dset_id, error)
  ! Terminate access to the data space.
  CALL H5SCLOSE_F(dspace_id, error)


  dsetname = 'id'
  data_dims = (/npart/)
  CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
  CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_INTEGER, dspace_id, &
          dset_id, error)
  CALL H5DWRITE_F(dset_id, H5T_NATIVE_INTEGER, id, data_dims, error)
  CALL H5DCLOSE_F(dset_id, error)
  CALL H5SCLOSE_F(dspace_id, error)


  do idim=1,3
     if (idim==1) dsetname = 'xpart'
     if (idim==2) dsetname = 'ypart'
     if (idim==3) dsetname = 'zpart'

     CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
     CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
             dset_id, error)
     select case (idim)
        case (1) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, xp , data_dims, error)
        case (2) 
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, yp , data_dims, error)
        case (3)
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, zp , data_dims, error)
     end select 
     CALL H5DCLOSE_F(dset_id, error)
     CALL H5SCLOSE_F(dspace_id, error)
  enddo

  ! Create an array for easy coding
  if(do_sngl)then
     if(allocated(ss)) deallocate(ss)
     allocate(ss(1:npart))
  else
     if(allocated(xx)) deallocate(xx)
     allocate(xx(1:npart))
  endif

  
  do idim=1,3
     if (idim==1) dsetname = 'vxp'
     if (idim==2) dsetname = 'vyp'
     if (idim==3) dsetname = 'vzp'
     CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
     if(do_sngl)then
        CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_REAL, dspace_id, &
                dset_id, error)
        if (idim==1) ss(:) = vxp(:)
        if (idim==2) ss(:) = vyp(:)
        if (idim==3) ss(:) = vzp(:)
        CALL H5DWRITE_F(dset_id, H5T_NATIVE_REAL, ss, data_dims, error)
     else
        CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
                dset_id, error)
        if (idim==1) xx(:) = vxp(:)
        if (idim==2) xx(:) = vyp(:)
        if (idim==3) xx(:) = vzp(:)
        CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, xx, data_dims, error)
     endif
     CALL H5DCLOSE_F(dset_id, error)
     CALL H5SCLOSE_F(dspace_id, error)
  end do

  data_dims = (/npart/)
  do ivar=1,nname_part
     dsetname = TRIM(dsetname_part(ivar))   ! mp,tp,metp,mp0

     okay=.true.
     if(ivar==3) then
        if(.not.do_metal) okay=.false.      ! if metp exists
     endif 
     if(ivar==4) then
        if(.not.do_initm) okay=.false.      ! if mp0 exists
     endif 

     if(okay) then

        CALL H5SCREATE_SIMPLE_F(rank, data_dims, dspace_id, error)
        if(do_sngl)then

           if(ivar==1) ss=mp
           if(ivar==2) ss=tp
           if(ivar==3) ss=metp
           if(ivar==4) ss=mp0

           CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_REAL, dspace_id, &
                 dset_id, error)
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_REAL, ss, data_dims, error)
           CALL H5DCLOSE_F(dset_id, error)
           CALL H5SCLOSE_F(dspace_id, error)

        else

           if(ivar==1) xx=mp
           if(ivar==2) xx=tp
           if(ivar==3) xx=metp
           if(ivar==4) xx=mp0

           CALL H5DCREATE_F(group_id, dsetname, H5T_NATIVE_DOUBLE, dspace_id, &
                 dset_id, error)
           CALL H5DWRITE_F(dset_id, H5T_NATIVE_DOUBLE, xx, data_dims, error)
           CALL H5DCLOSE_F(dset_id, error)
           CALL H5SCLOSE_F(dspace_id, error)

        endif ! do_sngl

     end if ! okay

  end do






  ! Close the file.
  CALL H5FCLOSE_F(file_id, error)

  ! Close FORTRAN interface.
  CALL H5CLOSE_F(error)


  if(allocated(ss))    deallocate(ss)
  if(allocated(xx))    deallocate(xx)

END PROGRAM RAMSES_HDF5
!#####################################################
!#####################################################
!#####################################################
