;+
; NAME:
;    RD_RAM
;
; PURPOSE:
;    This procedure reads HDF5 generated with RAMSES_HDF5.F90
;       and convert into something easy to handle
;
; CATEGORY:
;    Scientific data 
;
; CALLING SEQUENCE:
;    RD_RAM, NOUT=, CELL=, PART=, STAR=, INFO=, PATH=
; 
; COMMON BLOCKS:
;    None.
;
; REQUIRED INPUTS:
;    NOUT: snapshot number
;
; OPTIONAL INPUTS:
;    PATH: directory of HDF5
;    FILE: name of HDF5
;    VERBOSE: print messages
;
; MODIFICAITON HISTORY:
;    Written by: Taysun Kimm, Oct/13/2019
;-
pro rd_ram, cell=cell,info=info,part=part,star=star,nout=nout,$
            path=path,file=HDF5_file,silent=silent

	verbose = ~keyword_set(silent)

   col='\033[1;31m' ; red
   NC='\033[0m'

	;----------------------------
	; input check
	;----------------------------
	if n_elements(nout) eq 0 then begin
   	err_message='>>> ERROR: NOUT is missing'
   	comm = 'echo -e "'+col+err_message+NC+'"'
		spawn,comm
		retall
	endif

	;----------------------------
	; guess file name
	;----------------------------
	if n_elements(file) eq 0 then begin
		if n_elements(path) eq 0 then path='HDF5'
		snout = string(nout,format='(I05)')
		HDF5_file = path+'/ramses_'+snout+'.h5'
	endif 

	;----------------------------
	; check if file exists
	;----------------------------
	if ~file_test(HDF5_file) then begin
   	err_message='>>> ERROR: not found '+HDF5_file
   	comm = 'echo -e "'+col+err_message+NC+'"'
		spawn,comm
		retall
	endif


	;----------------------------
	; read HDF5 file
	;----------------------------
	if verbose then begin
		print, '>>> reading...'+HDF5_file	
	endif	
	h5 = H5_PARSE(HDF5_file,/read)
	
	

	;--------------------------------
	; retrieve info and re-structure 
	;--------------------------------
	info_old = h5.info._data ; structure
	header   = h5.cell.header._data ; variable #1...
	
	tags_old = tag_names(info_old)
	comm = 'info = {'
	for it=0,n_elements(tags_old)-1 do begin
		dtype = '0d0'
		if tags_old[it] eq 'LMIN' then dtype='0L'
		if tags_old[it] eq 'LMAX' then dtype='0L'
		if tags_old[it] eq 'NDIM' then dtype='0L'
		if tags_old[it] eq 'NCPU' then dtype='0L'
		comm += tags_old[it]+':'+dtype+','
	endfor
	
	tags_new = ['boxkpc','pc','msun']
	dtype = '0d0'
	for it=0,n_elements(tags_new)-1 do begin
		comm += tags_new[it]+':'+dtype
		if it ne n_elements(tags_new)-1 then comm +=','
	endfor

	comm += ', header:header, itemp:4L}'

	dum = execute(comm)
	
	for it=0,n_elements(tags_old)-1 do begin
		comm = 'info.'+tags_old[it]+'= info_old.'+tags_old[it]
		dum  = execute(comm)
	endfor

	info.boxkpc = info.boxpc/1d3
	info.pc     = info.boxpc
	info.msun   = info.unit_msun
	
	;------------------------------
	; determine itemp
	;------------------------------
	for i=0,n_elements(info.header)-1 do begin
		text = info.header[i]
		ok = strmatch(text,'*: thermal*')
		if ok then info.itemp = i
		ok = strmatch(text,'* pressure*')
		if ok then info.itemp = i
	endfor


	;------------------------------
	; retrieve basic info for cell
	;------------------------------
	nvar = h5.cell.nvar._data[0]
	ncell = h5.cell.x._nelements[0]
	x_prec = h5.cell.x._precision

	if x_prec eq 64 then begin
		xx = dblarr(ncell)
	endif else begin
		xx = fltarr(ncell)
	endelse

	var_prec = h5.cell.var00001._precision
	if var_prec eq 64 then begin
		cell = {x:xx,y:xx,z:xx,dx:xx,var:dblarr(ncell,nvar)}
	endif else begin
		cell = {x:xx,y:xx,z:xx,dx:xx,var:fltarr(ncell,nvar)}
	endelse

	;----------------------------
	; fetch hydro data
	;----------------------------
	cell.x  = h5.cell.x._data
	cell.y  = h5.cell.y._data
	cell.z  = h5.cell.z._data
	cell.dx = h5.cell.dx._data

	for ivar=1,nvar do begin

		si    = strtrim(ivar-1,2)
		COMM  = 'cell.var[*,'+si+']='

		svar  = 'var'+string(ivar,format='(I05)')
		COMM += 'h5.cell.'+svar+'._data'
	
		dum = execute(COMM)

	endfor

	;----------------------------
	; sanity check for hydro data
	;----------------------------
	for ivar=0,nvar-1 do begin
		ok = where( finite(cell.var[*,ivar]) eq 0, nok)
		if nok gt 0 then begin
			err_message='>>> ERROR: NaN found in ivar='+strtrim(ivar,2)+' ncell='+strtrim(nok,2)
			comm = 'echo -e "'+col+err_message+NC+'"'
			spawn,comm
		endif
	endfor
	
	;------------------------------
	; retrieve basic info for part
	;------------------------------
	ntot = h5.part.id._nelements

	tags = tag_names(h5.part)

	; check if TP field exists
	dum  = where(tags eq 'ZP', met_exist) ; either 0 or 1
	dum  = where(tags eq 'MP0', mp0_exist) ; either 0 or 1

	;---------------------------------------
	; fetch data space for star and DM
	;---------------------------------------
;	prec_part = h5.part.xpart._precision
;	if prec_part eq 64 then begin	
;		xp = dblarr(ntot,3)
;	endif else begin
;		xp = fltarr(ntot,3)
;	endelse
;	xp[*,0] = h5.part.xpart._data
;	xp[*,1] = h5.part.ypart._data
;	xp[*,2] = h5.part.zpart._data
;	vp = fltarr(ntot,3)
;	vp[*,0] = h5.part.vxp._data
;	vp[*,1] = h5.part.vyp._data
;	vp[*,2] = h5.part.vzp._data
;
;	id = h5.part.id._data
		
	;---------------------------------------
	; prepare data space for star and DM
	;---------------------------------------
	tp = h5.part.tp._data
	ok_star = where( abs(tp) gt 1d-20, nstar)
	ok_part = where( abs(tp) lt 1d-20, npart)
	do_star = nstar > 0
	do_part = npart > 0


	;---------------------------------------
	; take care of stars first
	;---------------------------------------
	option = 'npart:np, xp:dblarr(np,3),vp:fltarr(np,3),mp:fltarr(np),id:lonarr(np),tp:fltarr(np)'
	if met_exist then option += ',zp:fltarr(np)'
	if mp0_exist then option += ',mp0:fltarr(np)'

	np = nstar
	comm = 'star = {'+option+'}'

	if do_star then begin
		dum  = execute(comm)
		star.xp[*,0] = h5.part.xpart._data[ok_star]
		star.xp[*,1] = h5.part.ypart._data[ok_star]
		star.xp[*,2] = h5.part.zpart._data[ok_star]
		star.vp[*,0] = h5.part.vxp._data[ok_star]
		star.vp[*,1] = h5.part.vyp._data[ok_star]
		star.vp[*,2] = h5.part.vzp._data[ok_star]
		star.mp[*] = h5.part.mp._data[ok_star]
		star.tp[*] = h5.part.tp._data[ok_star]
		star.id[*] = h5.part.id._data[ok_star]
		if met_exist then star.zp[*]  = h5.part.zp._data[ok_star]
		if mp0_exist then star.mp0[*] = h5.part.mp0._data[ok_star]
	endif else begin
		np = 1
		dum  = execute(comm)
		star.npart=0	
	endelse

	;---------------------------------------
	; take care of other particles
	;---------------------------------------
	option = 'npart:np, xp:dblarr(np,3),vp:fltarr(np,3),mp:fltarr(np),id:lonarr(np),tp:fltarr(np)'
	np = npart
	comm = 'part = {'+option+'}'

	if do_part then begin
		dum  = execute(comm)
		part.xp[*,0] = h5.part.xpart._data[ok_part]
		part.xp[*,1] = h5.part.ypart._data[ok_part]
		part.xp[*,2] = h5.part.zpart._data[ok_part]
		part.vp[*,0] = h5.part.vxp._data[ok_part]
		part.vp[*,1] = h5.part.vyp._data[ok_part]
		part.vp[*,2] = h5.part.vzp._data[ok_part]
		part.mp[*] = h5.part.mp._data[ok_part]
		part.tp[*] = h5.part.tp._data[ok_part]
		part.id[*] = h5.part.id._data[ok_part]
	endif else begin
		np = 1
		dum  = execute(comm)
		part.npart=0	
	endelse



end
